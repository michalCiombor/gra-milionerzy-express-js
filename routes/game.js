function gameRoutes(app) {
  let goodAnswers = 0;
  let isGameOver = false;
  let callToAFriendUsed = false;
  let questionToTheCrowdUsed = false;
  let halfOnHalfUsed = false;

  const questions = [
    {
      question: "W jaki sposób dodasz zewnętrzny plik style.css?",
      answers: [
        `add style = "style.css"`,
        `link src = "style.css"`,
        `link rel="stylesheet" href="style.css"`,
        `find "style.css"`
      ],
      correctAnswer: 2
    },
    {
      question: "Co to jest Macintosh?",
      answers: [
        "System operacyjny",
        "Gra karciana",
        "Rodzaj pamięci RAM",
        "Złącze na płycie głównej"
      ],
      correctAnswer: 0
    },
    {
      question: "Kto stworzył HTML?",
      answers: ["Tim Cook", "Robert Cailliau", "Steve Jobs", "Putin"],
      correctAnswer: 1
    },
    {
      question: "Jaką mamy aktualnie werję HTML?",
      answers: ["HTML3", "HTML ES6", "HTML5", "HTML7"],
      correctAnswer: 2
    },
    {
      question: "Position absolute, pozycjonuje wzgędem?",
      answers: ["programisty", "okna przeglądarki", "rodzica", "potomka"],
      correctAnswer: 1
    },
    {
      question: "Jaka organizacja stoi za CSS",
      answers: ["Microsoft", "Google", "w3c", "Oracle"],
      correctAnswer: 2
    },
    {
      question: "Jak zmienić czcionkę elementu?",
      answers: [
        `add style = "style.css"`,
        `link src = "style.css"`,
        `link rel="stylesheet" href="style.css"`,
        `find "style.css"`
      ],
      correctAnswer: 2
    },
    {
      question: "Jak należy dodawać kolor tła dla WSZYSTKICH elementów <h1>?",
      answers: [
        "h1.all {background-color:#FFFFFF}",
        "Gra karciana",
        "Rodzaj pamięci RAM",
        "Złącze na płycie głównej"
      ],
      correctAnswer: 0
    },
    {
      question: "Jak zmienić kolor tekstu elementu?",
      answers: [". text-color=", "Robert Cailliau", "Steve Jobs", "Putin"],
      correctAnswer: 1
    },
    {
      question: "Która własność pozwala na ustalenie kolor tła?",
      answers: ["background-color:", "HTML ES6", "HTML5", "HTML7"],
      correctAnswer: 2
    },
    {
      question:
        "Które z podanych tagów pozwalają na dodawanie wewnętrznych stylów?",
      answers: ["programisty", "okna przeglądarki", "rodzica", "potomka"],
      correctAnswer: 1
    },
    {
      question: "Jak można wyświetlić odnośnik bez podkreślenia?",
      answers: ["a{text-decoration:none}", "Google", "w3c", "Oracle"],
      correctAnswer: 2
    }
  ];

  app.get("/question", (req, res) => {
    if (goodAnswers === questions.length) {
      res.json({
        winner: true
      });
    } else {
      let nextQuestion = questions[goodAnswers];
      let { question, answers } = nextQuestion;
      res.json({
        question,
        answers,
        goodAnswers,
        isGameOver
      });
    }
  });
  app.post("/answer/:index", (req, res) => {
    const { index } = req.params;
    const presentQuestion = questions[goodAnswers];
    const isGoodAnswer = Number(index) === presentQuestion.correctAnswer;

    if (isGoodAnswer) {
      goodAnswers++;
    } else {
      isGameOver = true;
    }
    if (isGameOver) {
      res.json({
        loser: true
      });
    } else {
      res.json({
        winner: isGoodAnswer,
        goodAnswers,
        isGameOver
      });
    }
  });

  //askAFriend
  app.get("/help/friend", (req, res) => {
    const friendAnswer = [
      "Przepraszam, ale nie wiem",
      `Wydaje mi się, że będzie to ${
        questions[goodAnswers].answers[questions[goodAnswers].correctAnswer]
      }`
    ];
    if (callToAFriendUsed) {
      return res.json({
        text: "Koło zostało już wykorzystane"
      });
    } else {
      callToAFriendUsed = true;
      res.json({
        text: `${friendAnswer[Math.floor(Math.random() * friendAnswer.length)]}`
      });
    }
  });
  //halfOnHalf
  app.get("/help/half", (req, res) => {
    if (halfOnHalfUsed) {
      return res.json({
        text: "Koło zostało już wykorzystane"
      });
    } else {
      halfOnHalfUsed = true;
      const question = questions[goodAnswers];
      let answersCopy2 = [];
      const answersCopy = question.answers.forEach((el, index) => {
        if (Number(index) !== questions[goodAnswers].correctAnswer) {
          answersCopy2.push(el);
        }
      });
      answersCopy2.splice(~~Math.random() * answersCopy2.length, 1);

      res.json({
        answersToRemove: answersCopy2
      });
    }
  });
  //questionToTheCrowd
  app.get("/help/crowd", (req, res) => {
    crowdAnswer = [
      questions[goodAnswers].answers[questions[goodAnswers].correctAnswer],
      "C"
    ];

    if (questionToTheCrowdUsed) {
      return res.json({
        text: "Koło zostało już wykorzystane"
      });
    } else {
      questionToTheCrowdUsed = true;
      res.json({
        text: `Publiczność wybrała odp: ${
          crowdAnswer[Math.floor(Math.random() * crowdAnswer.length)]
        }`
      });
    }
  });
  app.post("/newGame", (req, res) => {
    isGameOver = false;
    halfOnHalfUsed = false;
    questionToTheCrowdUsed = false;
    callToAFriendUsed = false;
    goodAnswers = 0;

    res.json({
      text: "GRA ZRESETOWANA"
    });
  });
}
module.exports = gameRoutes;
