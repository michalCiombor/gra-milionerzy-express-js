const express = require("express");
const path = require("path");
const app = express();
const gameRoutes = require("./routes/game");

app.listen(3500, () => {
  console.log(
    "serwer wystartował -- let's play a game :) gra dostępna pod adresem http://127.0.0.1:3500"
  );
});
app.use(express.static(path.join(__dirname, "static")));
gameRoutes(app);
