const question = document.querySelector("#question");
const buttons = document.querySelectorAll(".answer");
const goodAnswers = document.querySelector("#goodAnswers");
const gameBoard = document.querySelector("#game-board");
const h2 = document.querySelector("h2");
const lifeSaverButtons = document.querySelectorAll(".life-savers__button");
const buttonCall = document.querySelector("#callToAFriend");
const buttonHalf = document.querySelector("#halfOnHalf");
const buttonCrowd = document.querySelector("#questionToTheCrowd");
const tip = document.querySelector("#tip");
const buttonNewGame = document.querySelector("#newGame");

function showNextQuestion() {
  for (const button of buttons) {
    if (button.hasAttribute("disabled")) {
      button.removeAttribute("disabled");
    }
  }
  tip.innerText = "";
  fetch("/question", {
    method: "GET"
  })
    .then(r => r.json())
    .then(data => fillQuestionElements(data));
}

showNextQuestion();

function fillQuestionElements(data) {
  if (data.winner) {
    gameBoard.style.display = "none";
    h2.innerText = "WYGRAŁEŚ";
  } else if (data.isGameOver) {
    gameBoard.style.display = "none";

    h2.innerText = "PRZEGRAŁEŚ";
    setTimeout(newGame, 4000);
  } else {
    buttons.forEach(
      (button, index) => (button.innerText = data.answers[index])
    );
    question.innerText = data.question;
    goodAnswers.innerText = data.goodAnswers;
  }
}

function sendAnswer(answerIndex) {
  fetch(`/answer/${answerIndex}`, {
    method: "POST"
  })
    .then(r => r.json())
    .then(data => showNextQuestion(data.winner));
}
for (const button of buttons) {
  button.addEventListener("click", e => {
    const buttonIndex = e.target.dataset.answer;
    sendAnswer(buttonIndex);
  });
}
//MAIN FUNCTION FOR LIFE SAVERS
function callToAFriend(event) {
  switch (event.target.id) {
    case "callToAFriend": {
      this.classList.add("disabled");
      fetch("/help/friend", {
        method: "GET"
      })
        .then(r => r.json())
        .then(data => {
          tip.innerText = data.text;
        });
      break;
    }
    case "halfOnHalf":
      {
        this.classList.add("disabled");
        fetch("/help/half", {
          method: "GET"
        })
          .then(r => r.json())
          .then(data => {
            if (data.text) {
              tip.innerText = data.text;
            } else {
              let array = data.answersToRemove.map(el => el.toUpperCase());

              for (const button of buttons) {
                if (array.includes(button.innerText)) {
                  button.innerText = "-";
                  button.setAttribute("disabled", "true");
                }
              }
            }
          });
      }
      break;
    case "questionToTheCrowd": {
      this.classList.add("disabled");
      fetch("/help/crowd", {
        method: "GET"
      })
        .then(r => r.json())
        .then(data => {
          tip.innerText = data.text;
        });
      break;
    }
  }
}
function newGame(e) {
  showNextQuestion();
  h2.innerText = "";
  gameBoard.style.display = "flex";
  goodAnswers.innerText = 0;
  let lifeSaverDuplicate = Array.from(lifeSaverButtons);
  for (const button of lifeSaverDuplicate) {
    if (button.classList.contains("disabled")) {
      button.classList.remove("disabled");
    }
  }
  fetch("/newGame", {
    method: "POST"
  })
    .then(r => r.json())
    .then(data => {
      console.log(data);
    });
}

//LIFE SAVERS

buttonCall.addEventListener("click", callToAFriend);
buttonHalf.addEventListener("click", callToAFriend);
buttonCrowd.addEventListener("click", callToAFriend);
//NEW GAME

buttonNewGame.addEventListener("click", newGame);
